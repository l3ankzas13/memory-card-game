import { generateCards } from "utils/generate";

describe("generateCards", () =>
 {
  it("should generateCards is correctly", () => {
    const cards = generateCards()
    const cardsValue = cards.map(( { value }) => value)
    const cardValueLessThen13 = cardsValue.every((value) => value >= 1 && value <= 6)
    const getCardByNumber = (number) => cardsValue.filter((value) => value === number)

    expect(cards).toHaveLength(12);
    expect(cardValueLessThen13).toBe(true);
    expect(getCardByNumber(1)).toHaveLength(2);
    expect(getCardByNumber(2)).toHaveLength(2);
    expect(getCardByNumber(3)).toHaveLength(2);
    expect(getCardByNumber(4)).toHaveLength(2);
    expect(getCardByNumber(5)).toHaveLength(2);
    expect(getCardByNumber(6)).toHaveLength(2);
  });
});