import React from "react";

import { render, screen, fireEvent, within } from "../../test-utils";
import BoardGame from "components/BoardGame/index";
import { generateMockCards } from 'utils/generate';

describe("BoardGame component", () =>
 {
  it("should render BoardGame correctly", async () => {
    const mockOpenFn = jest.fn()
    const mockGetValueFn = jest.fn()
    const props = {
      data: generateMockCards(),
      onOpen: mockOpenFn,
      onGetValue: mockGetValueFn
    }
    render(<BoardGame  {...props} />);

    const cardElement = screen.getAllByTestId(/^card-/i);

    expect(cardElement).toHaveLength(12);
    expect(mockOpenFn).toHaveBeenCalledTimes(0)
    expect(mockGetValueFn).toHaveBeenCalledTimes(12)
  });

  it("should render BoardGame with click card is correctly", async () => {
    const mockOpenFn = jest.fn()
    const mockGetValueFn = jest.fn()
    const props = {
      data: generateMockCards(),
      onOpen: mockOpenFn,
      onGetValue: mockGetValueFn
    }
    render(<BoardGame  {...props} />);

    const cardElement = screen.getAllByTestId(/^card-/i);

    expect(cardElement).toHaveLength(12);

    await fireEvent.click(screen.getByTestId('card-1'))
    await fireEvent.click(screen.getByTestId('card-2'))
    await fireEvent.click(screen.getByTestId('card-3'))

    expect(mockOpenFn).toHaveBeenCalledTimes(3)
    expect(mockGetValueFn).toHaveBeenCalledTimes(12)
  });
});