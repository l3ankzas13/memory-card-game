import { uid } from './uid';

type CardsValue = {
  id: string,
  value: number
}

export const generateCards = (): Array<CardsValue>  => {
  const numbers = Array.from({ length: 6 }, (v, index) => index + 1)
    const cardNumber = numbers.concat(numbers);
    const cards = cardNumber.sort(() => Math.random() - 0.5).map((num, i) => ({ id: uid(), value: num }))
    return cards;
}

export const generateMockCards = (): Array<CardsValue>  => {
  const numbers = Array.from({ length: 6 }, (v, index) => index + 1)
    const cardNumber = numbers.concat(numbers);
    const cards = cardNumber.map((num, i) => ({ id: uid(), value: num })).sort((a, b) => a.value - b.value)
    return cards;
}