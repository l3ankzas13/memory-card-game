import { ReactNode, useCallback, useEffect, useState  } from 'react';
import { GetServerSideProps } from 'next'
import Head from 'next/head'

import useGame from 'hooks/useGame';
import BoardGame from 'components/BoardGame';
import Score from 'components/Score'

type MemoryGameProps = {
  globalBestScore: number
}

export default function MemoryGame({ 
  globalBestScore
}: MemoryGameProps) {

  const {
    cards,
    restart,
    clickCount,
    myBestScore,
    handleOpenCard,
    getCardValue
  } = useGame()

  return (
    <div className="container">
      <Head>
        <title>Card Memory Game</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Score
        clickCount={clickCount}
        myBestScore={myBestScore}
        globalBestScore={globalBestScore}
        onRestart={restart}
      />
      <BoardGame
        data={cards}
        onOpen={(id) => handleOpenCard(id)}
        onGetValue={(id) => getCardValue(id)}
      />
      <div className="box mt-10 show-sm-down">
          <button
            className="btn btn-primary"
            onClick={restart}>
               <span>New Game</span>
          </button>
      </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const baseUrl = process.env.BASE_API_URL
  let globalBestScore = 0
  let error
  try {
    const data = await (await fetch(`${baseUrl}/api/score/best`)).json()
    if (data?.score) globalBestScore = data?.score
  } catch (err) {
    console.log('error', error)
  }
  return {
    props: { globalBestScore }
  }
}
