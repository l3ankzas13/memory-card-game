import React from "react";
// Using render and screen from test-utils.js instead of
// @testing-library/react
import { render, screen, fireEvent, within } from "../test-utils";
import HomePage from "pages/index";

describe("HomePage", () =>
 {
  it("should render HomePage correctly", () => {
    render(<HomePage globalBestScore={12} />);

    const cardElement = screen.getAllByTestId(/^card-/i);
    const clickCount = screen.getByText('Click:');
    const myBestScore = screen.getByText('My Best:');
    const globalBestScore = screen.getByText('Global Best:');
    const newGameButton = screen.getAllByText('New Game');

    expect(cardElement).toHaveLength(12);
    expect(clickCount).toBeInTheDocument();
    expect(myBestScore).toBeInTheDocument();
    expect(globalBestScore).toBeInTheDocument();
    expect(newGameButton).toHaveLength(2);
  });

  it("should render initial score is correct", () => {
    render(<HomePage globalBestScore={15} />);

    const clickCount = screen.getByTestId('click-count');
    const myBestScore = screen.getByTestId('my-best-score');
    const globalBestScore = screen.getByTestId('global-best-score')

    expect(clickCount).toHaveTextContent('0');
    expect(myBestScore).toHaveTextContent('0');
    expect(globalBestScore).toHaveTextContent('15');
  });

  it("should has card value when click card", async () => {
    render(<HomePage globalBestScore={15} />);
    await fireEvent.click(screen.getByTestId('card-1'))
    const backCard =  screen.getByTestId('back-card-1');
    const backCard2 = screen.getByTestId('back-card-2');
    const clickCount = screen.getByTestId('click-count');
    expect(backCard.firstChild).not.toBeNull();
    expect(backCard2.firstChild).toBeNull();
    expect(clickCount).toHaveTextContent('1');
  });

  it("should has card value when click first card and last card", async () => {
    render(<HomePage globalBestScore={15} />);
    await fireEvent.click(screen.getByTestId('card-1'))
    const backCard =  screen.getByTestId('back-card-1');
    expect(backCard.firstChild).not.toBeNull();

    await fireEvent.click(screen.getByTestId('card-12'))
    const backCard12 = screen.getByTestId('back-card-12');
    expect(backCard12.firstChild).not.toBeNull();

    const backCard6 = screen.getByTestId('back-card-6');
    expect(backCard6.firstChild).toBeNull();

    const clickCount = screen.getByTestId('click-count');
    expect(clickCount).toHaveTextContent('2');
  });
});