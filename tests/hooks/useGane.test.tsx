import { renderHook, act } from '@testing-library/react-hooks/server'
import useGame from 'hooks/useGame'
import { generateMockCards } from 'utils/generate'

test('should render initial correctly', () => {
  const { result, hydrate } = renderHook(() => useGame())
  expect(result.current.cards).toHaveLength(0)
  expect(result.current.flippedIds).toHaveLength(0)
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(0)
  expect(result.current.myBestScore).toEqual(0)

  hydrate()

  expect(result.current.cards).toHaveLength(12)
  expect(result.current.flippedIds).toHaveLength(0)
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(0)
  expect(result.current.myBestScore).toEqual(0)
})


test('should render initial correctly', async () => {
  const { result, hydrate } = renderHook(() => useGame())
  hydrate()
  const card1 = result.current.cards[0]
  act(() => {
    result.current.handleOpenCard(card1.id)
  })
  expect(result.current.selectedCard).toEqual({ [card1.id]: card1.value })
  expect(result.current.flippedIds).toHaveLength(0)
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(1)
  expect(result.current.myBestScore).toEqual(0)
})

test('should block click when open two card', async () => {
  const { result, hydrate } = renderHook(() => useGame())
  hydrate()
  const card1 = result.current.cards[0]
  const card2 = result.current.cards[2]
  act(() => {
    result.current.handleOpenCard(card1.id)
    result.current.handleOpenCard(card2.id)
  })
  expect(result.current.selectedCard).toEqual({ 
    [card1.id]: card1.value,
    [card2.id]: card2.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
})

test('should block click when open all card is correctly', async () => {
  const { result, hydrate, waitForNextUpdate } = renderHook(() => useGame())
  hydrate()

  act(() => {
    const cards = generateMockCards()
    result.current.setCards(cards)
  })

  // setup card
  const card1 = result.current.cards[0]
  const card2 = result.current.cards[1]
  const card3 = result.current.cards[2]
  const card4 = result.current.cards[3]
  const card5 = result.current.cards[4]
  const card6 = result.current.cards[5]
  const card7 = result.current.cards[6]
  const card8 = result.current.cards[7]
  const card9 = result.current.cards[8]
  const card10 = result.current.cards[9]
  const card11 = result.current.cards[10]
  const card12 = result.current.cards[11]
  
  // open match 1
  act(() => {
    result.current.handleOpenCard(card1.id)
    result.current.handleOpenCard(card2.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card1.id]: card1.value,
    [card2.id]: card2.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)


  // open match 2
  act(() => {
    result.current.handleOpenCard(card3.id)
    result.current.handleOpenCard(card4.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card3.id]: card3.value,
    [card4.id]: card4.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  // open match 3
  act(() => {
    result.current.handleOpenCard(card5.id)
    result.current.handleOpenCard(card6.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card5.id]: card5.value,
    [card6.id]: card6.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(6)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(6)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(6)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(6)

  // open match 4
  act(() => {
    result.current.handleOpenCard(card7.id)
    result.current.handleOpenCard(card8.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card7.id]: card7.value,
    [card8.id]: card8.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(8)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(8)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(8)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(8)

  // open match 5
  act(() => {
    result.current.handleOpenCard(card9.id)
    result.current.handleOpenCard(card10.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card9.id]: card9.value,
    [card10.id]: card10.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(10)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(10)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(10)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(10)


  // open match 6
  act(() => {
    result.current.handleOpenCard(card11.id)
    result.current.handleOpenCard(card12.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card11.id]: card11.value,
    [card12.id]: card12.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(12)
  expect(result.current.myBestScore).toEqual(12)
  expect(result.current.flippedIds).toHaveLength(12)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(12)
  expect(result.current.myBestScore).toEqual(12)
  expect(result.current.flippedIds).toHaveLength(12)
})


test('should block click when open all card is correctly with my best score is 14', async () => {
  const { result, hydrate, waitForNextUpdate } = renderHook(() => useGame())
  hydrate()

  act(() => {
    const cards = generateMockCards()
    result.current.setCards(cards)
  })

  //setup card
  const card1 = result.current.cards[0]
  const card2 = result.current.cards[1]
  const card3 = result.current.cards[2]
  const card4 = result.current.cards[3]
  const card5 = result.current.cards[4]
  const card6 = result.current.cards[5]
  const card7 = result.current.cards[6]
  const card8 = result.current.cards[7]
  const card9 = result.current.cards[8]
  const card10 = result.current.cards[9]
  const card11 = result.current.cards[10]
  const card12 = result.current.cards[11]
  
  // open match 1
  act(() => {
    result.current.handleOpenCard(card1.id)
    result.current.handleOpenCard(card2.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card1.id]: card1.value,
    [card2.id]: card2.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)


  // open match 2
  act(() => {
    result.current.handleOpenCard(card3.id)
    result.current.handleOpenCard(card4.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card3.id]: card3.value,
    [card4.id]: card4.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  // open match 3
  act(() => {
    result.current.handleOpenCard(card5.id)
    result.current.handleOpenCard(card6.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card5.id]: card5.value,
    [card6.id]: card6.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(6)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(6)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(6)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(6)

  // open match 4
  act(() => {
    result.current.handleOpenCard(card7.id)
    result.current.handleOpenCard(card8.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card7.id]: card7.value,
    [card8.id]: card8.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(8)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(8)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(8)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(8)

  // open match 5
  act(() => {
    result.current.handleOpenCard(card9.id)
    result.current.handleOpenCard(card10.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card9.id]: card9.value,
    [card10.id]: card10.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(10)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(10)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(10)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(10)

   // open match 6 (with wrong case)
   act(() => {
    result.current.handleOpenCard(card11.id)
    result.current.handleOpenCard(card1.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card11.id]: card11.value,
    [card1.id]: card1.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(12)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(10)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(12)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(10)

  // open match 7
  act(() => {
    result.current.handleOpenCard(card11.id)
    result.current.handleOpenCard(card12.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card11.id]: card11.value,
    [card12.id]: card12.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(14)
  expect(result.current.myBestScore).toEqual(14)
  expect(result.current.flippedIds).toHaveLength(12)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(14)
  expect(result.current.myBestScore).toEqual(14)
  expect(result.current.flippedIds).toHaveLength(12)
})



test('should block click when open card 2 match and restart is correctly', async () => {
  const { result, hydrate, waitForNextUpdate } = renderHook(() => useGame())
  hydrate()

  act(() => {
    const cards = generateMockCards()
    result.current.setCards(cards)
  })

  //setup card
  const card1 = result.current.cards[0]
  const card2 = result.current.cards[1]
  const card3 = result.current.cards[2]
  const card4 = result.current.cards[3]
  const card5 = result.current.cards[4]
  const card6 = result.current.cards[5]
  
  // open match 1
  act(() => {
    result.current.handleOpenCard(card1.id)
    result.current.handleOpenCard(card2.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card1.id]: card1.value,
    [card2.id]: card2.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)


  // open match 2
  act(() => {
    result.current.handleOpenCard(card3.id)
    result.current.handleOpenCard(card4.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card3.id]: card3.value,
    [card4.id]: card4.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  // restart game
  act(() => {
    result.current.restart()
  })

  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(0)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(0)
})


test('should block click when open card 2 match and getCardValue is correctly', async () => {
  const { result, hydrate, waitForNextUpdate } = renderHook(() => useGame())
  hydrate()

  act(() => {
    const cards = generateMockCards()
    result.current.setCards(cards)
  })

  //setup card
  const card1 = result.current.cards[0]
  const card2 = result.current.cards[1]
  const card3 = result.current.cards[2]
  const card4 = result.current.cards[3]
  const card5 = result.current.cards[4]
  const card6 = result.current.cards[5]
  
  // open match 1
  act(() => {
    result.current.handleOpenCard(card1.id)
    result.current.handleOpenCard(card2.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card1.id]: card1.value,
    [card2.id]: card2.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(2)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(2)


  // open match 2
  act(() => {
    result.current.handleOpenCard(card3.id)
    result.current.handleOpenCard(card4.id)
  })
  
  expect(result.current.selectedCard).toEqual({ 
    [card3.id]: card3.value,
    [card4.id]: card4.value
  })
  expect(result.current.clickBlocked).toBe(true)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  await waitForNextUpdate({ timeout: 2000 })
  expect(result.current.selectedCard).toEqual({})
  expect(result.current.clickBlocked).toBe(false)
  expect(result.current.clickCount).toEqual(4)
  expect(result.current.myBestScore).toEqual(0)
  expect(result.current.flippedIds).toHaveLength(4)

  // get card value by cardId
  act(() => {
    const cardValue = result.current.getCardValue(card3.id)
    expect(cardValue).toEqual(card3.value)
  })

})