import React from "react";

import { render, screen, fireEvent } from "../../test-utils";
import Card from "components/BoardGame/card";
import { generateMockCards } from 'utils/generate';

describe("Card component", () =>
 {
  it("should render Card correctly", async () => {
    const mockOpenFn = jest.fn()
    const props = {
      testId: 'card-1',
      id: 'card-1',
      display: '',
      onClick: mockOpenFn
    }
    const { rerender } = render(<Card {...props} />);

    const cardFront  = screen.getByTestId(`front-${props.testId}`)
    const cardBack = screen.getByTestId(`back-${props.testId}`)
    const cardBody = screen.getByTestId(`body-${props.testId}`)

    expect(mockOpenFn).toHaveBeenCalledTimes(0)
    expect(cardBody).toBeInTheDocument()
    expect(cardFront).toBeInTheDocument()
    expect(cardBack).toBeInTheDocument()
    expect(cardBack.firstChild).toBeNull()

    const propsRerender = {
      testId: 'card-1',
      id: 'card-1',
      display: '2',
      onClick: mockOpenFn
    }
    rerender(<Card {...propsRerender} />)

    expect(mockOpenFn).toHaveBeenCalledTimes(0)
    expect(cardBody).toBeInTheDocument()
    expect(cardFront).toBeInTheDocument()
    expect(cardBack).toBeInTheDocument()
    expect(cardBack.firstChild).not.toBeNull()
  });
});