import {  useState, useEffect, useCallback, useRef } from 'react';
import { generateCards } from 'utils/generate'

type CardsState = {
  id: string,
  value: number
}

const delay = (ms = 1000) => {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

const useGame = () => {
  const isMounted = useRef(false)
  const [cards, setCards] = useState<Array<CardsState>>([])
  const [flippedIds, setFlippedIds] = useState<Array<string|null>>([])
  const [selectedCard, setSelectedCard] = useState<object>({})
  const [clickBlocked, setBlockClick] = useState<boolean>(false)
  const [clickCount, setClickCount] = useState<number>(0)
  const [myBestScore, setMyBestScore] = useState<number>(0)

  useEffect(() => {
    if (flippedIds?.length === 12) {
      if (myBestScore === 0 || clickCount < myBestScore) {
        setMyBestScore(clickCount)
      }
    }
  }, [flippedIds])

  useEffect(() => {
    isMounted.current = true;
    const setSelectCard = async () => {
      const data = Object.entries(selectedCard)
      if (data.length === 2) {
        setBlockClick(true)
        if (data[0][1] === data[1][1]) {
          setFlippedIds((prev) => [...prev, data[0][0], data[1][0]])
        }  
        await delay(1000)
        if (isMounted.current) {
          setSelectedCard({})
          setBlockClick(false)
        }
      }
    } 
    setSelectCard()
    return () => { isMounted.current = false };
  }, [selectedCard])

  useEffect(() => {
    start()
  }, [])

  const start = useCallback(() => {
    const cards = generateCards()
    setCards(cards)
  }, [cards])

  const restart = useCallback(() => {
    setCards([])
    setFlippedIds([])
    setSelectedCard({})
    setClickCount(0)
    start()
  }, [])


  const handleOpenCard = (cardId: string): void => {
    if (clickBlocked) return;
    setClickCount(count => count + 1)
    const data = cards.find(({ id }) => id === cardId)
    const hasInCurrent = selectedCard[cardId]
    if (!hasInCurrent && data?.value) setSelectedCard((prevData) => ({ ...prevData, [cardId]: data?.value }))
  }

  const getCardValue = (cardId: string): string => {
    const flippedId = flippedIds.find((flipId) => flipId === cardId)
    const cardValue = cards.find(({ id }) => flippedId === id)?.value
    return selectedCard[cardId] || cardValue
  }

  return {
    cards,
    flippedIds,
    selectedCard,
    clickBlocked,
    clickCount,
    myBestScore,
    setCards,
    setFlippedIds,
    setSelectedCard,
    setBlockClick,
    setClickCount,
    setMyBestScore,
    handleOpenCard,
    getCardValue,
    restart,
    start
  } as const
}

export default useGame;