import React, { ReactNode } from 'react';
import CardItem from './card'

type CardsState = {
  id: string,
  value: number | string
}


type Props = {
  data: Array<CardsState>,
  onOpen: (value: string) => void,
  onGetValue: (value: string) => string,
}

export default function BoardGame({ 
  data,
  onOpen,
  onGetValue
}: Props) {

  return (
    <div className="cards mt-10">
      {data.map(({ id }, index) => (
        <CardItem
          testId={`card-${index+1}`}
          key={id} 
          id={id}
          display={onGetValue(id)}
          onClick={(id) => onOpen(id)}
        />
      ))}
  </div>
  )
}