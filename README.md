# Memory Card Game

### Demo: (Run on digital ocean) [Go to demo](https://memory-card-game-aekawan.vercel.app/)

<br/>

## Menu
- [Run project on localhost with dev](#localhost-dev)
- [Run project on localhost with docker](#localhost-docker)
- [How to deploy to server](#how-to-deploy)



<br/>

##  <a name="localhost-dev">Run project on localhost with dev</a>
<br/>

- step1: Clone project and goto project
```
git clone https://gitlab.com/l3ankzas13/memory-card-game.git
cd memory-card-game
```

- step2: Install dependencies
```
yarn install
```

- step3: Start with dev environment
```
yarn dev
```

- step4: Run test
```
yarn test
```

- step5: Go to website
```
http://localhost:3000
```
<br/>
<br/>

## <a name="localhost-docker">Run project on localhost with docker (have a nginx reverse proxy)</a>
<br/>

- step1: Clone project and goto project
```
git clone https://gitlab.com/l3ankzas13/memory-card-game.git
cd memory-card-game
```

- step:2 Start Docker with production environment
```
docker-compose up -d
```

- step3: Log monitor
```
docker-compose logs --follow
```

- step4: Go to website
```
http://localhost
```
<br/>
<br/>


# <a name="how-to-deploy">How to deploy to server (Go to your server)</a>
<br/>

## Step1: Install tools (Git, Docker and Docker Compose)
<br/>

### Install GIT on Ubuntu

See Full Step: https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04

- step1:
```
sudo apt update
```

- step2:
```
sudo apt install git
```

- step3:
```
git --version
```
<br/>

### Install Docker on ubuntu

See Full Step: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04

- step1:
```
sudo apt update

```
- step2:
```
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```
- step3:
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

- step4:
```
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
```

- step5:
```
apt-cache policy docker-ce
```

- step: 6
```
sudo apt install docker-ce
```

- step 7: type docker for checking installation
```
docker
```
<br/>


### Install Docker Compose on ubuntu

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04

- step1:
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

```
- step2:
```
sudo chmod +x /usr/local/bin/docker-compose
```
- step3:
```
docker-compose --version
```

## Step2: set up project and run on docker
<br/>

- step1: Clone Project on Git repository and go to project

```
git clone https://gitlab.com/l3ankzas13/memory-card-game.git
cd memory-card-game
```

- step2: project with Docker compose:

```
docker-compose up -d
```


- step3: Log monitor:

```
docker-compose logs --follow
```


- step4: Go to website with server ip:

```
http://[your-server-ip]

etc. http://165.232.166.106
```

