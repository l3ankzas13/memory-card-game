export const uid = (): string => {
  const str = Date.now().toString(36) + Math.random().toString(36).substr(2);
  return str.substring(0, 17)
}