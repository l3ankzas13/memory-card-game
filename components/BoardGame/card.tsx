import React from 'react';
import classnames from 'classnames';

type Props = {
  id: string,
  testId: string,
  display: string | null,
  onClick: (value: string) => void;
}

export default function CardComponent({ testId, id, display, onClick }: Props) {
  const handleClick = (): void => {
    if (!display) onClick(id)
  }

  return (
    <div
      data-testid={testId}
      className="card"
      onClick={handleClick}
    >
      <div
        data-testid={`body-${testId}`}
        className={classnames('card-body', { active: !!display })}
       >
        <div
          className="card-front"
          data-testid={`front-${testId}`}
        />
        <div
          className="card-back"
          data-testid={`back-${testId}`}>
            {display}
        </div>
      </div>
    </div>
  )
}