import { uid } from "utils/uid";

describe("uid", () =>
 {
  it("should uid is correctly", () => {
    const uidValue = uid()
    expect(uidValue).toHaveLength(17);
  });
});