import React from "react";
import { render, screen, fireEvent } from "../../test-utils";
import Score from "components/Score/index";

describe.only("Score component", () =>
 {
  
  it("should render is correct", async () => {
    const mockCallBack = jest.fn();
    const props = {
      clickCount: 0,
      myBestScore: 0,
      globalBestScore: 12,
      onRestart: mockCallBack,
    }

    render(<Score {...props} />);

    const clickCount = screen.getByTestId('click-count');
    const myBestScore = screen.getByTestId('my-best-score');
    const globalBestScore = screen.getByTestId('global-best-score')

    expect(clickCount).toHaveTextContent('0');
    expect(myBestScore).toHaveTextContent('0');
    expect(globalBestScore).toHaveTextContent('12');

    await fireEvent.click(screen.getByTestId('btn-new-game'))
    expect(mockCallBack).toHaveBeenCalledTimes(1)
  });
});