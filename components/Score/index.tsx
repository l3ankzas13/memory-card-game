type ScoreProps = {
  clickCount: number,
  myBestScore: number,
  globalBestScore: number,
  onRestart: () => void
};
export default function Score({ 
  clickCount,
  myBestScore,
  globalBestScore,
  onRestart
}: ScoreProps) {
  return (
    <div className="content">
        <div className="box score">
          <div className="text mt-20">
            <span className="title">Click:</span>
            <span data-testid="click-count">{clickCount}</span>
          </div>
          <div className="text mt-20">
            <span className="title">My Best:</span>
            <span  data-testid="my-best-score">{myBestScore}</span>
          </div>
          <div className="text mt-20">
            <span className="title">Global Best:</span>
            <span data-testid="global-best-score">{globalBestScore}</span>
          </div>
        </div>
        <div className="box mt-20 hidden-sm-up">
          <button
            data-testid="btn-new-game"
            className="btn btn-primary"
            onClick={onRestart}>
              <span>New Game</span>
          </button>
        </div>
      </div>
  )
}